// Default empty project template
import bb.cascades 1.0
import bb.data 1.0

TabbedPane {
    showTabsOnActionBar: true
    Menu.definition: MenuDefinition {
        // Specify the actions that should be included in the menu
        actions: [
            ActionItem {
                title: "About"
                imageSource: "images/info.png"

                onTriggered: {

                }
            },
            ActionItem {
                title: "Logout"
                imageSource: "images/logout.png"

                onTriggered: {

                }
            }
        ]
    }

    peekEnabled: false

    Tab {
        title: "Feed"
        imageSource: "images/exp.png"
        Page {

            actions: [
                ActionItem {
                    title: "Refresh"
                    imageSource: "images/ref.png"
                    onTriggered: {
                        dataSource.load()
                    }
                }
            ]

            Container {
                layout: StackLayout {

                }

                ListView {
                    dataModel: dataModel

                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            ItemView {
                            }
                        }
                    ]
                }

            }

            attachedObjects: [
                DataSource {
                    id: dataSource
                    onDataLoaded: {
                        //helpText.text = "Loaded json"
                        dataModel.clear()
                        dataModel.insertList(data)
                    }
                },
                GroupDataModel {
                    id: dataModel
                }
            ]
            onCreationCompleted: {
                dataSource.source = "http://www.lepsta.com/index.php/api/"
                dataSource.load()
            }
        }
    }

    Tab {
        title: "Explore"
        imageSource: "images/feed.png"
        onTriggered: {
            gridSource.source = "http://www.lepsta.com/index.php/api/"
            gridSource.load()
        }

        Page {
            id: explorePage
            actions: [
                ActionItem {
                    title: "Refresh"
                    imageSource: "images/ref.png"
                }
            ]
            Container {
                layout: StackLayout {

                }

                ListView {
                    layout: GridListLayout {
                        columnCount: 2

                    }
                    dataModel: gridModel
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            GridView {
                            }
                        }
                    ]
                    horizontalAlignment: HorizontalAlignment.Fill
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1.0
                    }
                }

            }

            attachedObjects: [
                DataSource {
                    id: gridSource
                    onDataLoaded: {
                        gridModel.clear()
                        gridModel.insertList(data)
                    }
                },

                GroupDataModel {
                    id: gridModel
                }
            ]
            onCreationCompleted: {

            }
        }
    }
    Tab {
        title: "Search"
        imageSource: "images/search.png"
        Page {
            id: searchPage
        }
    }
    Tab {
        title: "Liked"
        imageSource: "images/pop.png"
        Page {
            id: likesPage
        }
    }

}