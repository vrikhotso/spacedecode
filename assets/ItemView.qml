import bb.cascades 1.0
import org.labsquare 1.0
import bb.data 1.0

Container {
    id: imageItem
    Container {
        layout: StackLayout {
        }
        
        leftPadding: 10
        rightPadding: 10
        topPadding: 10
        bottomPadding: 5

        Label {
            text: "People in Space"
            textStyle.base: SystemDefaults.TextStyles.SubtitleText
            multiline: true
        }

        WebImageView {
            id: img
            minWidth: 750
            minHeight: 750
            url: ListItemData.url
            horizontalAlignment: HorizontalAlignment.Center
            scalingMethod: ScalingMethod.AspectFill
            contextActions: [
                ActionSet {
                    ActionItem {
                        title: "like"
                        imageSource: "images/pop.png"
                    }

                    InvokeActionItem {
                        query {
                            mimeType: "text/plain"
                            invokeActionId: "bb.action.SHARE"
                        }

                        onTriggered: {
                            data = ListItemData.url
                        }
                        objectName: "share"
                    }

                    ActionItem {
                        title: "comment"
                        imageSource: "images/com.png"
                    }
                }
            ]
        }

        leftMargin: 10.0
        Label {
            text: ListItemData.description
            textStyle.base: SystemDefaults.TextStyles.SubtitleText
            multiline: true
            textStyle.color: Color.DarkGray
        }
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight

            }
            
            ImageView {
                imageSource:"asset:///images/sample.png"
            }
            Label {
                text: ListItemData.comments
                textStyle.color: Color.DarkGray
            }
            
            ImageView {
                imageSource: "asset:///images/sample.png"
            }
            Label {
                text: ListItemData.likes
                textStyle.color: Color.DarkGray
            }
        }

    }
    Divider {
    }
}