import bb.cascades 1.0
import org.labsquare 1.0
import bb.data 1.0

Container {
    WebImageView {
        id: img
        minWidth: 300
        minHeight: 300
        url: ListItemData.url
        horizontalAlignment: HorizontalAlignment.Center
        scalingMethod: ScalingMethod.AspectFill
        preferredWidth: 300.0
        preferredHeight: 300.0
        maxWidth: 300.0
        maxHeight: 300.0
    }
}